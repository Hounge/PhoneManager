package com.android.internal.telephony;

import android.os.Bundle;

public abstract interface ITelephony
{
    public abstract void answerRingingCall();

    public abstract void call(String paramString);

    public abstract void cancelMissedCallsNotification();

    public abstract void dial(String paramString);

    public abstract int disableApnType(String paramString);

    public abstract boolean disableDataConnectivity();

    public abstract void disableLocationUpdates();

    public abstract int enableApnType(String paramString);

    public abstract boolean enableDataConnectivity();

    public abstract void enableLocationUpdates();

    public abstract boolean endCall();

    public abstract int getActiveCallsCount();

    public abstract int getActivePhoneType();

    public abstract long getCallBaseTime();

    public abstract int getCallState();

    public abstract long getCallTime();

    public abstract String getCallerName();

    public abstract int getCdmaEriIconIndex();

    public abstract int getCdmaEriIconMode();

    public abstract String getCdmaEriText();

    public abstract boolean getCdmaNeedsProvisioning();

    public abstract Bundle getCellLocation();

    public abstract int getDataActivity();

    public abstract int getDataState();

    public abstract boolean getDataStatebyApnType(String paramString);

    public abstract int getHoldCallsCount();

    public abstract boolean getMsisdnavailable();

    public abstract boolean getMute();

    public abstract int getNetworkType();

    public abstract int getSimPinRetry();

    public abstract int getSimPukRetry();

    public abstract int getVoiceMessageCount();

    public abstract boolean handlePinMmi(String paramString);

    public abstract boolean hasIccCard();

    public abstract void initiateFakecall();

    public abstract boolean isDataConnectivityPossible();

    public abstract boolean isIdle();

    public abstract boolean isOffhook();

    public abstract boolean isRadioOn();

    public abstract boolean isRinging();

    public abstract boolean isSimFDNEnabled();

    public abstract boolean isSimPinEnabled();

    public abstract boolean isVideoCall();

    public abstract void setMute(boolean paramBoolean);

    public abstract boolean setRadio(boolean paramBoolean);

    public abstract boolean showCallScreen();

    public abstract boolean showCallScreenWithDialpad(boolean paramBoolean);

    public abstract void silenceRinger();

    public abstract boolean supplyPin(String paramString);

    public abstract boolean supplyPin2(String paramString);

    public abstract boolean supplyPuk(String paramString1, String paramString2);

    public abstract void switchHoldingAndActive();

    public abstract void toggleRadioOnOff();

    public abstract void turnOnSpeaker(boolean paramBoolean);

    public abstract void updateServiceLocation();
}
