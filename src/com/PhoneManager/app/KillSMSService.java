package com.PhoneManager.app;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;


/**
 * Created by hounge on 8/15/14.
 */
public class KillSMSService extends Service {

    private final Handler handler = new Handler();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
            this.handler.postDelayed(this.KillSMS, 100);
            return Service.START_NOT_STICKY;
    }

    private Runnable KillSMS = new Runnable() {
        public void run() {
            KillSMSService.this.killThisPackageIfRunning(this,"com.android.mms");
            KillSMSService.this.killThisPackageIfRunning(this,"com.google.android.talk");
            KillSMSService.this.handler.postDelayed(this, 100);
        }
    };

    @SuppressLint("NewApi")
    private void killThisPackageIfRunning(Runnable runnable, String packageName) {
            ActivityManager activityManager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
            activityManager.killBackgroundProcesses(packageName);
    }
    
    @Override
    public void onDestroy() {
            stopSelf();
            super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
            //TODO for communication return IBinder implementation
            return null;
    }
}
