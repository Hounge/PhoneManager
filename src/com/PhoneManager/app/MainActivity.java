package com.PhoneManager.app;


import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.*;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity implements
        CompoundButton.OnCheckedChangeListener {
         static final String TAG = "DevicePolicyActivity";
         static final int ACTIVATION_REQUEST = 47;
         DevicePolicyManager devicePolicyManager;
         ComponentName DeviceAdmin;
         ToggleButton toggleButton;
         ToggleButton toggleButton1;
         ToggleButton toggleButton2;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        toggleButton = (ToggleButton) super
                .findViewById(R.id.adminbutton);
        toggleButton.setOnCheckedChangeListener(this);

        toggleButton1 = (ToggleButton) super
                .findViewById(R.id.callbutton);
        toggleButton1.setOnCheckedChangeListener(this);

        toggleButton2 = (ToggleButton) super
                .findViewById(R.id.smsbutton);
        toggleButton2.setOnCheckedChangeListener(this);

        devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        DeviceAdmin = new ComponentName(this, AdminReceiver.class);

    }

    public void callbuttonClick(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            enablePhoneCallReceiver();
            enableOutgoingCallListener();
            Toast.makeText(this, "Blocking Calling", Toast.LENGTH_LONG).show();
            Log.d(TAG, "Locking Calling now");
        } else {
            try {
                disablePhoneCallReceiver();
                Toast.makeText(this, "Blocking Calling...", Toast.LENGTH_LONG).show();
                Log.d(TAG, "Locking Calling now");
            } catch (IllegalArgumentException e) {

              }
            try{
                disableOutgoingCallListener();
            }catch (IllegalArgumentException e){
             }
          }
    }
    
    public void enablePhoneCallReceiver() {
    ComponentName receiver = new ComponentName(this, PhoneCallReceiver.class);
    PackageManager pm = this.getPackageManager();

    pm.setComponentEnabledSetting(receiver,
    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
    PackageManager.DONT_KILL_APP);
    Toast.makeText(this, "Enabled broadcast receiver", Toast.LENGTH_SHORT).show();
    }
    
    public void enableOutgoingCallListener() {
        ComponentName receiver = new ComponentName(this, PhoneCallReceiver.class);
        PackageManager pm = this.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
        Toast.makeText(this, "Enabled broadcast receiver", Toast.LENGTH_SHORT).show();
    }

    public void disablePhoneCallReceiver(){
        ComponentName receiver = new ComponentName(this, PhoneCallReceiver.class);
        PackageManager pm = this.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
        Toast.makeText(this, "Disabled broadcast receiver", Toast.LENGTH_SHORT).show();
    }

    public void disableOutgoingCallListener(){
        ComponentName receiver = new ComponentName(this, OutgoingCallListener.class);
        PackageManager pm = this.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
        Toast.makeText(this, "Disabled broadcast receiver", Toast.LENGTH_SHORT).show();
    }


    public void smsbuttonClick(View view) {
        boolean on = ((ToggleButton) view).isChecked();
        if (on) {
            Intent i = new Intent(MainActivity.this, KillSMSService.class);
            startService(i);
            Toast.makeText(this, "Blocking Texting", Toast.LENGTH_LONG).show();
            Log.d(TAG, "Blocking Texting now");
        } else {
            Intent i = new Intent(MainActivity.this, KillSMSService.class);
            stopService(i);
            Toast.makeText(this, "Allow Texting", Toast.LENGTH_LONG).show();
            Log.d(TAG, "Allow Texting now");
        }

    }

    @Override
    public void onCheckedChanged(CompoundButton button, boolean isChecked) {
        if (isChecked) {
                // Activate device administration
                Intent intent = new Intent(
                    DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                    DeviceAdmin);
                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                    "Control Manager Needs Permissions");
                startActivityForResult(intent, ACTIVATION_REQUEST);
        }
                Log.d(TAG, "onCheckedChanged to: " + isChecked);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
                case ACTIVATION_REQUEST:
                 if (resultCode == Activity.RESULT_OK) {
                    Log.i(TAG, "Administration enabled!");
                    toggleButton.setChecked(true);
                    } else {
                        Log.i(TAG, "Administration enable FAILED!");
                        toggleButton.setChecked(false);
                }
                return;
        }
         super.onActivityResult(requestCode, resultCode, data);
    }

}
