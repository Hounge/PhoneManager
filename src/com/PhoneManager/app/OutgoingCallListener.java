package com.PhoneManager.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

/**
 * Created by hounge on 8/22/14.
 */
public class OutgoingCallListener extends BroadcastReceiver {

    public void onReceive(final Context context, Intent intent) {
        Toast.makeText(context, "Blocking Call", Toast.LENGTH_LONG).show();
        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

            if (incomingNumber == null) {
                Runtime rt = Runtime.getRuntime();
                try {
                    Runtime.getRuntime().exec("service call phone 5");
                } catch (IOException ex) {
                    Log.d("CallBlock", "IOException: " + ex.toString());
                    setResultData(null);

                  }
            }
        }
    }
}

